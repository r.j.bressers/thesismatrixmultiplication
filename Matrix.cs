using System;
using System.Linq;

namespace ThesisMatrixMultiplication
{
    class Matrix
    {
        public int Dimension;
        public int[][] Elements;

        public Matrix(int dimension, bool generateElements = false)
        {
            Dimension = dimension;
            Elements = new int[Dimension][];

            for (int i = 0; i < Dimension; i++) 
            { Elements[i] = new int[Dimension]; }

            if (generateElements) 
            { GenerateElements(); }
        } 

        public void SetElement(int row, int column, int element)
        {
            Elements[row][column] = element;
        }

        public ref int GetElement(int row, int column)
        {
            return ref Elements[row][column];
        }

        public void SetElements(int[][] elements)
        {
            for (int i = 0; i < elements.Length; i++)
            {
                for (int j = 0; j < elements[i].Length; j++)
                {
                    Elements[i][j] = elements[i][j];
                }
            }
        }

        public void SetElements(Matrix[][] quadrants)
        {
            int n = (Dimension + 1) / 2;

            for (int x = 0; x < 2; x++)
            {   
                for (int y = 0; y < 2; y++) 
                {
                    for (int s = 0; s < n; s++)
                    {   
                        for (int t = 0; t < n; t++)
                        {
                            int i = n * x + s;
                            int j = n * y + t;
                            if (i < Dimension && j < Dimension)
                            {
                                Elements[i][j] = quadrants[x][y].GetElement(s, t);
                            }
                        }
                    }
                } 
            }
        }

        public int[][] GetElements()
        {
            int[][] elements = new int[Dimension][];

            for (int i = 0; i < Dimension; i++)
            {
                elements[i] = Elements[i].ToArray();
            }

            return elements;
        }

        public Matrix[][] GetQuadrants()
        {
            int n = (Dimension + 1) / 2;

            Matrix[][] quadrants = new Matrix[2][];

            for (int x = 0; x < 2; x++)
            {
                quadrants[x] = new Matrix[2];

                for (int y = 0; y < 2; y++) 
                {
                    quadrants[x][y] = new Matrix(n);

                    for (int s = 0; s < n; s++)
                    {   
                        for (int t = 0; t < n; t++)
                        {
                            int i = n * x + s;
                            int j = n * y + t;

                            int element = i < Dimension && j < Dimension ? Elements[i][j] : 0;
                            quadrants[x][y].SetElement(s, t, element);
                        }
                    }
                } 
            }

            return quadrants;
        }

        public void GenerateElements(int maxValue = 9999)
        {
            int[][] elements = new int[Dimension][];

            for (int i = 0; i < Dimension; i++)
            {
                elements[i] = new int[Dimension];

                for (int j = 0; j < Dimension; j++)
                {
                    elements[i][j] = Program.rng.Next(maxValue) + 1;
                }
            }
            SetElements(elements);
        }

        public bool Equals(Matrix B)
        {
            int n = B.Dimension;
            if (Dimension != n) { return false; }

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (GetElement(i, j) != B.GetElement(i, j)) { return false; }
                }
            }
            return true;
        }

        public static Matrix operator + (Matrix A, Matrix B)
        {
            int n = A.Dimension;
            Matrix C = new Matrix(n);

            for (int i = 0; i < n; i++)
            {   
                for (int j = 0; j < n; j++)
                {
                    C.SetElement(i, j, A.GetElement(i, j) + B.GetElement(i, j));
                }
            }
            return C;
        }
       
        public static Matrix operator - (Matrix A, Matrix B)
        {
            int n = A.Dimension;
            Matrix C = new Matrix(n);

            for (int i = 0; i < n; i++)
            {   
                for (int j = 0; j < n; j++)
                {
                    C.SetElement(i, j, A.GetElement(i, j) - B.GetElement(i, j));
                }
            }
            return C;
        }

        public static Matrix operator * (Matrix A, Matrix B)
        {
            int n = A.Dimension;
            Matrix C = new Matrix(n);

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    int element = A.GetElement(i, 0) * B.GetElement(0, j);

                    for (int k = 1; k < n; k++)
                    {
                        element += A.GetElement(i, k) * B.GetElement(k, j);
                    }

                    C.SetElement(i, j, element);
                }
            }
            return C;
        }
    }
}
