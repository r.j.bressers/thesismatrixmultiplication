using System;
using System.Linq;
using System.Diagnostics;

namespace ThesisMatrixMultiplication
{
    class Program
    {
        public static Random rng;
        public static Stopwatch sw;

        public static int n0;

        static void Main(string[] args)
        {
            rng = new Random(0);
            sw = new Stopwatch();

            Matrix A = new Matrix(128, true);
            Matrix B = new Matrix(128, true);
            
            Matrix C = A * B;
            Matrix D = Strassen(A, B);
            
            Console.WriteLine(C.Equals(D));
        }

        public static Matrix Strassen(Matrix A, Matrix B)
        {
            int n = A.Dimension;

            if (n < n0)
            {
                return A * B;
            }

            Matrix C = new Matrix(A.Dimension);

            Matrix[][] Aqs = A.GetQuadrants().ToArray();
            Matrix[][] Bqs = B.GetQuadrants().ToArray();

            Matrix M1 = Strassen(Aqs[0][0] + Aqs[1][1] , Bqs[0][0] + Bqs[1][1]);
            Matrix M2 = Strassen(Aqs[1][0] + Aqs[1][1] , Bqs[0][0]);
            Matrix M3 = Strassen(Aqs[0][0] , Bqs[0][1] - Bqs[1][1]);
            Matrix M4 = Strassen(Aqs[1][1] , Bqs[1][0] - Bqs[0][0]);
            Matrix M5 = Strassen(Aqs[0][0] + Aqs[0][1] , Bqs[1][1]);
            Matrix M6 = Strassen(Aqs[1][0] - Aqs[0][0] , Bqs[0][0] + Bqs[0][1]);
            Matrix M7 = Strassen(Aqs[0][1] - Aqs[1][1] , Bqs[1][0] + Bqs[1][1]);

            Matrix C11 = M1 + M4 - M5 + M7;
            Matrix C12 = M3 + M5;
            Matrix C21 = M2 + M4;
            Matrix C22 = M1 - M2 + M3 + M6;

            Matrix[][] Cqs = new Matrix[][] { new Matrix[] { C11, C12 }, 
                                              new Matrix[] { C21, C22 } };
            C.SetElements(Cqs);

            return C;
        }
    }
}
